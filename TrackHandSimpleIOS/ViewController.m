//
//  ViewController.m
//  TrackHandSimpleIOS
//
//  Created by yichao on 2019/5/13.
//  Copyright © 2019 Hauoli. All rights reserved.
//

#import "ViewController.h"

#import "HauoliConst.h"
#import "HauoliTracker.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    //-------------------------------
    // Initialization
    int nMic = 1;
    int nSpk = 1;
    int nDim = 2;
    double *initPos = (double *)malloc(sizeof(double)*nDim);
    double *spkPos  = (double *)malloc(sizeof(double)*nSpk*nDim);
    double *micPos  = (double *)malloc(sizeof(double)*nMic*nDim);

    // set the initial distance from hand to mic = 100mm
    initPos[0] = 0;
    initPos[1] = -100; 
    // the position of the speaker on iPhone: [0, 0]
    spkPos[0] = 0;    
    spkPos[1] = 0;
    // the position of the microhpone on iPhone: [0, 0]
    micPos[0] = 0; 
    micPos[1] = 0;

    HauoliTracker *hauoliTracker = [[HauoliTracker alloc] init];
    [hauoliTracker initTracker:nSpk num_mic:nMic init_pos:initPos spk_pos:spkPos mic_pos:micPos];

    //-------------------------------
    // Other Configuration (don't need to change)
    [hauoliTracker setDistEstMethod:METHOD_TAP];
    [hauoliTracker setAudioSource:AUDIO_SOURCE_USER];
    [hauoliTracker setSeq:30 num_seq_up:240 b:6000];
    [hauoliTracker setMinTap:10];
    [hauoliTracker setMaxTap:200];
    [hauoliTracker setTapStep:20];
    [hauoliTracker setSkipTime:1.5];
    [hauoliTracker setResetMethod:RESET_NO];
    [hauoliTracker setAutoThrd:TAP_MAG_THRD_FIX thrd:0];
    // [hauoliTracker setAutoThrd:TAP_MAG_THRD_CALI thrd:0];
    // [hauoliTracker setAutoThrd:TAP_SMOOTH_THRD_FIX thrd:0.00001];
    [hauoliTracker setAbsDistMagThrd:0.00004];
    [hauoliTracker setAbsDistDifThrd:100];
    [hauoliTracker setAbsDistItvThrd:1];
    [hauoliTracker setTapSelection:TAP_SEL_MAG];
    [hauoliTracker setRecordAudio:FALSE];
    [hauoliTracker setSaveDist:FALSE];
    [hauoliTracker setUseFile:FALSE];
    [hauoliTracker enableGesture:FALSE];

    //-------------------------------
    // Start tracking
    [hauoliTracker start];
    while(TRUE) {
        int state = [hauoliTracker getState];

        if(state != TRACKER_STATE_TRACKING) {
            continue;
        }

        double dist  = [hauoliTracker getDist];
        int gest     = [hauoliTracker getGesture];
        int gest_cnt = [hauoliTracker getGestCnt];
        NSLog(@"dist = %f, gesture = %d (%d)", dist, gest, gest_cnt);

        [NSThread sleepForTimeInterval:0.05];
    }
    [hauoliTracker stop];

}


@end
